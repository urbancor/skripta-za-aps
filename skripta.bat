echo off

if not exist %3 mkdir %3 rem %3 je tretji argument podan po skripta.bat (torej preverjam ce je mapa za rezultate ze ustvarjena)
set result=0
echo.
echo Compiling: Naloga%1.java
echo.
javac Naloga%1.java

FOR /l %%A IN (1,1,10) DO (  rem v for loopu mora imeti indeks s katerim se sprehajamo dva %-ta
	
	
	call :izpisprvi 	rem klicem funkcijo, ki mi zacetni cas pretvori v stotinke
	java Naloga%1 %2I_%%A.txt %3output%%A.txt
	call :izpisdrugi 	rem klicem funkcijo, ki mi koncni cas pretvori v stotinke

	rem call :odstej starttime endtime 		rem funkcija, ki izracuna razliko in cas pretvori v sekunde in stotinke
		
	fc %3output%%A.txt %2O_%%A.txt > nul

	if errorlevel 1 (
	
		if %%A EQU 10 (
			echo | set /p= Test %%A: [91mWRONG[37m  
			call :odstej starttime endtime	
		) else (
			echo | set /p= Test %%A:  [91mWRONG[37m  
			call :odstej starttime endtime	
		)	

	) else (
		if %%A EQU 10 (
			echo | set /p= Test %%A: [92mOK[37m  
			call :odstej starttime endtime	
			set /a result+=1
		) else (
			echo | set /p= Test %%A:  [92mOK[37m  
			call :odstej starttime endtime	
			set /a result+=1
		)
		
	)
)
echo.
set /a procent=%result%*10
echo Points: %result%/10 [%procent% %%]

goto :eof

:izpisprvi
rem parsamo zacetni cas
set /a starture=1%time:~0,2%
set /a startmin=1%time:~3,2%
set /a startsec=1%time:~6,2%
set /a startsto=1%time:~9,2%


rem cas pretvorimo v stotinke
set /a starttime=(%starture%*60*60*100)+(%startmin%*60*100)+(%startsec%*100)+%startsto%
set /a starttime=1%starttime%

goto :eof

:izpisdrugi
rem parsamo zacetni cas
set /a endure=1%time:~0,2%
set /a endmin=1%time:~3,2%
set /a endsec=1%time:~6,2%
set /a endsto=1%time:~9,2%

rem cas pretvorimo v stotinke
set /a endtime=(%endure%*60*60*100)+(%endmin%*60*100)+(%endsec%*100)+%endsto%
set /a endtime=1%endtime%

goto :eof

:odstej

set /a start=%1
set /a end=%2
rem izracunamo razliko med zacetnim in koncnim casom
set /a razlika=(%end%-%start%)
rem pretvorimo v sekunde
set /a sectaken=%razlika% / 100
rem in stotinke
set /a stotaken=(%razlika%) %% 100
rem ce je stotink manj kot 10 moramo pri zapisu dodati 0
if %stotaken% LSS 10 (
	echo [%sectaken%,0%stotaken% s]
) else (
	echo [%sectaken%,%stotaken% s]
)


goto :eof