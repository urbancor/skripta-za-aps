Skripta, ki testira vse testne primere in preveri pravilnost izhodov.
Dane testne vhode shranimo v mapo, ki se nahaja v istem repozitoriju kot naš java program in skripta.
Program se mora imenovati NalogaX.java.
skripto poganjamo z naslednjimi parametri:

- številka naloge
- ime mape s testnimi vhodi in izhodi (na koncu dodamo backslash)
- (opcijsko) dodamo še ime mape v katero želimo shraniti rezultate(prav tako zaključimo z backslash)

primer:
imeskripte.bat 2 testi2\ rezultati\